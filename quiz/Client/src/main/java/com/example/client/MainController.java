package com.example.client;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MainController {
    @FXML
    Button startbtn;
    protected DataInputStream ins;
    protected DataInputStream ins2;
    protected DataOutputStream out;
    @FXML
    private Label question;
    @FXML
    private Rectangle QueRec;
    @FXML
    private RadioButton answer1;
    @FXML
    private  RadioButton answer2;
    @FXML
    private RadioButton answer3;
    Calc_que calc_que;

    @FXML
    private Rectangle Rec1;
    @FXML
    private Rectangle Rec2;
    @FXML
    private Rectangle Rec3;
    @FXML
    private  Rectangle Recbtn;



    @FXML
    void initialize() {
        try {
            Socket socket = new Socket("127.0.0.1", 11111);
            ins = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            ins2 = new DataInputStream(socket.getInputStream());
        } catch (IOException var7) {
            throw new RuntimeException(var7);
        }

        answer1.setVisible(false);
        answer2.setVisible(false);
        answer3.setVisible(false);
        QueRec.setVisible(false);
        Rec1.setVisible(false);
        Rec2.setVisible(false);
        Rec3.setVisible(false);
    }

    public void Answer1(ActionEvent actionEvent) throws IOException {
        if (answer1.isSelected()) {
                out.writeUTF(answer1.getText());
        }
    }
    public void Answer2(ActionEvent actionEvent) throws IOException {
        out.writeUTF(answer2.getText());
    }

    public void Answer3(ActionEvent actionEvent) throws IOException {
        out.writeUTF(answer3.getText());
    }
    public void Start() throws IOException {
        if (calc_que != null){
            calc_que.interrupt();
        }

        calc_que = new Calc_que(ins, question, answer1, answer2, answer3, Rec1, Rec2, Rec3);

        calc_que.start();

        startbtn.setVisible(false);
        Recbtn.setVisible(false);
        answer1.setVisible(true);
        answer2.setVisible(true);
        answer3.setVisible(true);
        QueRec.setVisible(true);
        Rec1.setVisible(true);
        Rec2.setVisible(true);
        Rec3.setVisible(true);
    }
}