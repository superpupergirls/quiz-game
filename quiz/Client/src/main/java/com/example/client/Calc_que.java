package com.example.client;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.shape.Rectangle;

import java.io.*;
import java.util.List;

public class Calc_que extends Thread {

    private DataInputStream ins;

    private Label que;

    private Label end;

    private RadioButton button_answer1;

    private RadioButton button_answer2;

    private RadioButton button_answer3;

    private Rectangle Rec1;
    private Rectangle Rec2;
    private Rectangle Rec3;

    Calc_que(DataInputStream ins, Label que, RadioButton button_answer1, RadioButton button_answer2, RadioButton button_answer3, Rectangle Rec1,Rectangle Rec2,Rectangle Rec3 ) {
        this.ins = ins;
        this.que = que;
        this.button_answer1 = button_answer1;
        this.button_answer2 = button_answer2;
        this.button_answer3 = button_answer3;
        this.end = end;
        this.Rec1 = Rec1;
        this.Rec2 = Rec2;
        this.Rec3 = Rec3;
    }

    @Override
    public void run() {
        ToggleGroup group = new ToggleGroup();
        button_answer1.setToggleGroup(group);
        button_answer2.setToggleGroup(group);
        button_answer3.setToggleGroup(group);

        String question;
        String answer1;
        String answer2;
        String answer3;

        List<String> input;

        try {
            ObjectInputStream obj = new ObjectInputStream(ins);
            input = (List) obj.readObject();
        } catch (ClassNotFoundException | IOException e) {
            throw new RuntimeException(e);
        }

        for (String str : input) {
            String[] arr2 = str.split(";");
            question = arr2[0];
            answer1 = arr2[1];
            answer2 = arr2[2];
            answer3 = arr2[3];

            String finalQuestion = question;
            String finalAnswer = answer1;
            String finalAnswer1 = answer2;
            String finalAnswer2 = answer3;
            Platform.runLater(() -> {
                this.que.setText(finalQuestion);
                this.button_answer1.setText(finalAnswer);
                this.button_answer2.setText(finalAnswer1);
                this.button_answer3.setText(finalAnswer2);
            });

            synchronized (this) {
                try {
                    wait(10000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        try {
            DataInputStream input2 = new DataInputStream(ins);
            String result = input2.readUTF();
            System.out.println(result);

            Platform.runLater(() -> {
                this.que.setText(result);
                this.button_answer1.setVisible(false);
                this.button_answer2.setVisible(false);
                this.button_answer3.setVisible(false);
                this.Rec1.setVisible(false);
                this.Rec2.setVisible(false);
                this.Rec3.setVisible(false);
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

