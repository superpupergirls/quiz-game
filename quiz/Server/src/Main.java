import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ServerSocket ss = null;
        try {
            System.out.println("Start");
            ss = new ServerSocket(11111);
            System.out.println("Waiting connection...");
            while (true) {
                Socket s = ss.accept();
                System.out.println("Client connected");
                ClientHandler clientSock = new ClientHandler(s);

                new Thread(clientSock).start();
                }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (ss != null) {
                try {
                    ss.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static class ClientHandler implements Runnable {
        private final Socket clientSocket;

        public ClientHandler(Socket socket)
        {
            this.clientSocket = socket;
        }

        public void run()
        {
            List<String> input = new ArrayList<>();
            input.add("Кто стал первым программистом в мире?;Ада Лавлейс;Чарльз Бэббидж;Джон фон Нейман");
            input.add("Какую клавишу нужно нажать, чтобы открыть код web-страницы?;F5;F11;F12");
            input.add("Уравнение, которое помимо функции содержит её производные называется;дифференциальное уравнение;иррациональное уравнение;тригонометрическое уравнение");
            input.add("Для дифференциального уравнения (d^2 x)/(dt^2)+8dx/dt+16x=0 характеристическое уравнение имеет вид;λ^2 + 8λ = 0;λ^2 + 8λ + 16 = 0;(λ — 4)^2 = 0");
            input.add("U = sin (xy). Тогда частная производная второго порядка (∂^2 u)/(∂x∂y) равна;cos(xy) — x*y*sin(xy);cos(x) + x*y*sin(xy);sin(xy) — x*y*cos(xy)");
            input.add("Если в квадратной матрице все её элементы, стоящие ниже или выше главной диагонали равны 0, то эта матрица называется;Единичной;Треугольной;Нулевой");
            input.add("В евклидовом пространстве матрица перехода от одного ортонормированного базиса к другому является;Ортогональной;Диагональной;Единичной");
            input.add("Какая формула подходит для того чтобы рассчитать число сочетаний без повторений?;n!/k!(n-k)!;n!/(k-n)!;(n-k)!");
            input.add("Закон дистрибутивности конъюнкции по отношению к дизъюнкции имеет вид;x v (yz) = (x v y)(x v z);xy = yx;x(y v z) = xy v xz");
            input.add("Случайные события А и В - несовместимы. Тогда выполнено;p(A)+p(B) = 1;p(A+B) < 1;p(A) + p(B) ≤ 1");
            input.add("Тест закончился, нажмите на любую кнопку из оставшихся;1;2;3");


            DataInputStream ins = null;
            DataOutputStream ouuuts = null;
            ObjectOutputStream outs = null;
            int count = 0;
            String kva = " ";
            String res = " ";

            try {
                ins = new DataInputStream(clientSocket.getInputStream());
                outs = new ObjectOutputStream(clientSocket.getOutputStream());
                ouuuts = new DataOutputStream(clientSocket.getOutputStream());

                outs.writeObject(input);

                while(!(kva.equals("1") || kva.equals("2") || kva.equals("3"))) {

                    kva = ins.readUTF();

                    System.out.println(kva);

                    if (kva.equals("Ада Лавлейс")) {
                        count += 1;
                    }
                    if (kva.equals("F12")) {
                        count += 1;
                    }
                    if (kva.equals("дифференциальное уравнение")) {
                        count += 1;
                    }
                    if (kva.equals("λ^2 + 8λ + 16 = 0")) {
                        count += 1;
                    }
                    if (kva.equals("cos (xy) — xy sin (xy)")) {
                        count += 1;
                    }
                    if (kva.equals("Треугольной")) {
                        count += 1;
                    }
                    if (kva.equals("Ортогональной")) {
                        count += 1;
                    }
                    if (kva.equals("n!/k!(n-k)!")) {
                        count += 1;
                    }
                    if (kva.equals("x(y v z) = xy v xz")) {
                        count += 1;
                    }
                    if (kva.equals("p(A) + p(B) ≤ 1")) {
                        count += 1;
                    }
                }
                System.out.println(count);

                res = "Вы ответили правильно на " +count +" вопроса/ов";
                ouuuts.writeUTF(res);
                System.out.println(res);

            } catch (IOException e) {
                e.printStackTrace();
            } /*finally {
                try {
                    if (outs != null) {
                        outs.close();
                        if (ins != null) {
                        }
                        ins.close();
                        System.out.println("Клиент отключился");
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }*/
           // }
        }
    }
}